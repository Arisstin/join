import { TestBed, inject } from '@angular/core/testing';
import { MainFormServices } from './main-form.services';

describe('MainFormServices', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MainFormServices]
    });
  });

  it('should be created', inject([MainFormServices], (service: MainFormServices) => {
    expect(service).toBeTruthy();
  }));
});
