import { Component, OnInit, ViewChild, ElementRef, NgZone} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MainFormServices } from './main-form.services';
import { ObjectTypes } from './main-form.classes';
import { Hotel } from './main-form.classes';
import { TabsetComponent } from 'ngx-bootstrap';
import { MapsAPILoader } from '@agm/core';
import {} from '@types/googlemaps';
import {NotificationsService} from 'angular2-notifications';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

declare var google: any;

@Component({
  selector: 'app-main-form',
  templateUrl: './main-form.component.html',
  styleUrls: ['./main-form.component.scss']
})
export class MainFormComponent implements OnInit {
  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]{1,}\.[a-z]{2,4}$';
  phoneMask = ['+', /\d/, /\d/, ' ', '(', /\d/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  hotelForm: FormGroup;
  stepsComplete = false;
  hotelInfo: Hotel;
  isRequirePlace = false;
  typeObject: ObjectTypes;
  formAddress = '';
  formCity = '';
  notify_options: any;
  checkPhone = true;
  checkEmail = true;
  errArray: any[];
  sending = false;
  testTranslate: string[];
  public latitude: number;
  public longitude: number;
  public zoom: number;
  @ViewChild('search')
  public searchElementRef: ElementRef;

  constructor(private service: MainFormServices,
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone,
              private notificationservice: NotificationsService,
              private translate: TranslateService) {}
  ngOnInit() {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get(['thx_for_add', 'text_arror', 'address_not_found']).subscribe(res => {
        this.testTranslate = res;
      });
      this.service.getObjectTypes().subscribe(res => {
        this.typeObject = res;
      });
    });
    this.notify_options = {
      position: ['middle', 'center'],
      animate: 'fromBottom',
      showProgressBar: false
    };
    this.zoom = 8;
    this.latitude = 50.4507781;
    this.longitude = 30.523686099999964;
    // this.setCurrentPosition();
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.setNewLocation(this.latitude, this.longitude);
          this.zoom = 12;
        });
      });
    });

    this.hotelInfo = new Hotel;
    this.hotelForm = new FormGroup({
      'titleUa': new FormControl(this.hotelInfo.titleUa, [Validators.required]),
      'titleEn': new FormControl(this.hotelInfo.titleEn, [Validators.required]),
      'titleRu': new FormControl(this.hotelInfo.titleRu, [Validators.required]),
      'type': new FormControl(this.hotelInfo.type, Validators.required),
      'stars': new FormControl(this.hotelInfo.type, Validators.required),
      'roomCount': new FormControl(this.hotelInfo.roomCount, [Validators.required, Validators.min(0)]),
      'phone': new FormControl(this.hotelInfo.phone, [Validators.minLength(18), Validators.required]),
      'email': new FormControl(this.hotelInfo.email, [Validators.pattern(this.emailPattern), Validators.required]),
      'password': new FormControl(this.hotelInfo.password, [Validators.minLength(8), Validators.required]),
      'passwordconfirm': new FormControl(this.hotelInfo.passwordconfirm, [Validators.minLength(8), Validators.required]),
      'country': new FormControl(this.hotelInfo.country),
      'regionId': new FormControl(this.hotelInfo.regionId),
      'cityId': new FormControl(this.hotelInfo.cityId),
      'address': new FormControl(this.hotelInfo.address),
      'lat': new FormControl(this.hotelInfo.lat),
      'lng': new FormControl(this.hotelInfo.lng)
    }, {validators: this.matchingPasswords('password', 'passwordconfirm')});
  }
  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): {
      [key: string]: any
    } => {
      const pass = group.controls[passwordKey];
      const confirm = group.controls[confirmPasswordKey];

      if (pass.value !== confirm.value) {
        return {
          mismatchedPasswords: true
        };
      }
    };
  }
  setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.setNewLocation(this.latitude, this.longitude);
        this.zoom = 12;
      });
    }
  }
  mapClicked($event) {
    this.longitude = $event.coords.lng;
    this.latitude = $event.coords.lat;
    this.setNewLocation(this.latitude, this.longitude);
  }
  markerDragEnd($event) {
    this.longitude = $event.coords.lng;
    this.latitude = $event.coords.lat;
    this.setNewLocation(this.latitude, this.longitude);
  }
  setNewLocation(latitude: number, longitude: number) {
    const latlng = new google.maps.LatLng(latitude, longitude);
    const request = { latLng: latlng };
    const geocoder = new google.maps.Geocoder();
    geocoder.geocode(request, (results, status) => {
      if (status === google.maps.GeocoderStatus.OK) {
        if (results[0] !== null && results[0] !== undefined) {
          const mapCity = results.find(x => x.types[0] === 'locality');
          const mapRegion = results.find(x => x.types[0] === 'administrative_area_level_1');
          const mapCountry = results.find(x => x.types[0] === 'country');
          if (mapRegion !== undefined) {
            this.hotelForm.patchValue({
              'country': mapCountry.address_components[0].short_name,
              'lat': latitude,
              'lng': longitude,
              'regionId': mapRegion.place_id,
              'address': results[0].formatted_address,
              'cityId': mapCity ? mapCity.place_id : ''
            });
            this.formAddress = results[0].formatted_address;
            this.formCity = results[results.length - 1].formatted_address;
          } else { alert(this.testTranslate['address_not_found']); }
        } else {
          alert(this.testTranslate['address_not_found']);
        }
      } else { alert(this.testTranslate['address_not_found']); }
    });
  }

  get titleUa() { return this.hotelForm.get('titleUa'); }
  get titleEn() { return this.hotelForm.get('titleEn'); }
  get titleRu() { return this.hotelForm.get('titleRu'); }
  get type() { return this.hotelForm.get('type'); }
  get stars() { return this.hotelForm.get('stars'); }
  get roomCount() { return this.hotelForm.get('roomCount'); }
  get phone() { return this.hotelForm.get('phone'); }
  get email() { return this.hotelForm.get('email'); }
  get password() { return this.hotelForm.get('password'); }
  get passwordconfirm() { return this.hotelForm.get('passwordconfirm'); }
  get country() { return this.hotelForm.get('country'); }
  get regionId() { return this.hotelForm.get('regionId'); }
  get cityId() { return this.hotelForm.get('cityId'); }
  get address() { return this.hotelForm.get('address'); }
  get lat() { return this.hotelForm.get('lat'); }
  get lng() { return this.hotelForm.get('lng'); }
  selectTab(tab_id: number) {
    this.staticTabs.tabs[tab_id].active = true;
  }
  enableTab(tab_id: number) {
    this.staticTabs.tabs[tab_id].disabled = false;
  }
  cleanCheck() {
    this.checkPhone = true;
    this.checkEmail = true;
  }
  checkPhoneUniq() {
    this.service.checkPhone(this.hotelForm.value.phone).subscribe(res => {
      this.checkPhone = res.isUnique;
    });
  }
  checkEmailUnig() {
    this.service.checkEmail(this.hotelForm.value.email).subscribe(res => {
      this.checkEmail = res.isUnique;
    });
  }
  secondStep() {
    this.titleEn.markAsTouched({ onlySelf: true });
    this.titleUa.markAsTouched({ onlySelf: true });
    this.titleRu.markAsTouched({ onlySelf: true });
    this.roomCount.markAsTouched({ onlySelf: true });
    this.type.markAsTouched({ onlySelf: true });
    this.stars.markAsTouched({ onlySelf: true });
    if (this.titleEn.valid && this.titleRu.valid && this.titleUa.valid && this.roomCount.valid && this.stars.valid && this.type.valid) {
      this.enableTab(1);
      this.selectTab(1);
    }
  }
  thirdStep() {
    this.phone.markAsTouched({onlySelf: true});
    this.email.markAsTouched({onlySelf: true});
    this.password.markAsTouched({onlySelf: true});
    this.passwordconfirm.markAsTouched({onlySelf: true});
    if (this.phone.valid && this.password.valid && this.email.valid && !this.hotelForm.hasError('mismatchedPasswords')) {
      this.enableTab(2);
      this.selectTab(2);
      this.stepsComplete = true;
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  onFormSubmit() {
    if (this.hotelForm.value.cityId === '' && this.hotelForm.value.regionId === '') {
      this.isRequirePlace = true;
    } else {
      this.isRequirePlace = false;
    }
    this.validateAllFormFields(this.hotelForm);
    console.log(this.hotelForm.value);
    if (this.phone.invalid || this.password.invalid || this.email.invalid) {
      this.selectTab(1);
    }
    if (this.titleEn.invalid || this.titleUa.invalid || this.titleRu.invalid || this.roomCount.invalid || this.stars.invalid || this.type.invalid) {
      this.selectTab(0);
    }
    if (this.hotelForm.valid) {
      this.sending = true;
      this.service.postJoinInfo(this.hotelForm.value).subscribe(res => {
        this.sending = false;
        this.notificationservice.success('', this.testTranslate['thx_for_add']);
      }, err => {
        this.sending = false;
        let errormess = '';
        this.errArray = err.error;
        this.errArray.forEach(item => {
          if (item.property_path !== 'regionId' && item.property_path !== 'cityId' && item.property_path !== 'country' && item.property_path !== 'lat' && item.property_path !== 'lng' && item.property_path !== 'address') {
            errormess += '<br>' + item.message;
          }
        });
        const ind = this.errArray.findIndex(x => (x.property_path === 'regionId' || x.property_path === 'country' || x.property_path === 'cityId' || x.property_path === 'lat' || x.property_path === 'lng' || x.property_path === 'address'));
        if (ind !== -1) {
          errormess += '<br>' + this.testTranslate['address_not_found'];
        }
        this.notificationservice.error('', this.testTranslate['text_arror'] + errormess);
      });
    }
  }
}
