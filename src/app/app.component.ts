import { Component } from '@angular/core';
import { Globals } from '../services/globals';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  main_href: string;
  language = '';
  langs = ['ru', 'ua', 'en'];
  constructor(private globals: Globals, private translate: TranslateService) {
    this.main_href = this.globals.MAIN_HREF;
    translate.setDefaultLang('ru');
    const lang = this.langs.findIndex(x => x === translate.getBrowserLang());
    if (lang > -1) { translate.use(translate.getBrowserLang()); } else { this.translate.use('ua'); }
    this.translate.get('lang').subscribe(res => this.language = res);
  }
  langChanged(value: string) {
    this.translate.use(value);
    this.translate.get('lang').subscribe(res => this.language = res);
  }
}
