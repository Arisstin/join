import { Injectable } from '@angular/core';
import { Observable} from 'rxjs/Observable';
import { BaseService } from '../../services/base';
import { ObjectTypes } from './main-form.classes';

@Injectable()
export class MainFormServices extends BaseService {

  getObjectTypes(): Observable<ObjectTypes> {
    return this.get('hotel-types');
  }
  postJoinInfo(data: any): Observable<any> {
    return this.post('join', data);
  }
  checkEmail(data: string): Observable<any> {
    const post = {email: data};
    return this.post('join/check-email-uiqueness', post);
  }
  checkPhone(data: string): Observable<any> {
    const post = {phone: data};
    return this.post('join/check-phone-uiqueness', post);
  }
}
