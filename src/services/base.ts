import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { Globals} from './globals';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class BaseService {

  constructor(
    private http: HttpClient,
    private globals: Globals,
    private activeroute: ActivatedRoute,
    private translate: TranslateService
  ) {
  }

  get(url: string): Observable<any> {
    return this.http.get<any>(this.globals.BACK_END + this.translate.currentLang + '/' + url);
  }
  post(url: string, data: any): Observable<any> {
    return this.http.post<any>(this.globals.BACK_END + this.translate.currentLang + '/' + url, data);
  }
  put(url: string, data: any): Observable<any> {
    return this.http.post<any>(this.globals.BACK_END + this.translate.currentLang + '/' + url, data);
  }

}
