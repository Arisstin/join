export class Hotel {
  titleUa = '';
  titleEn = '';
  titleRu = '';
  type = '';
  stars = '';
  roomCount = '';
  phone = '';
  email = '';
  password = '';
  passwordconfirm = '';
  country = '';
  regionId = '';
  cityId = '';
  address = '';
  lat = '';
  lng = '';
}
export class ObjectTypes {
  types: ObjectType[];
}
export class ObjectType {
  id: number;
  title: string;
}
